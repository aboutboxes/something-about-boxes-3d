﻿using UnityEngine;
using System.Collections;

public class LightScript : MonoBehaviour {

	public GameObject obj;
	public GameObject obj1;
	public GameObject obj2;

    public static int lastState = 0;

    // Use this for initialization
    void Start () {
	
	}

	// Update is called once per frame
	void Update () {

		int numLights = ButtonBehaviors.scoreController.checkLights();
		//Debug.Log (numLights);
		// Get num of lights to turn on.
		switch (numLights) {
		case 0:
			turnOn (obj, false);
			turnOn (obj1, false);
			turnOn (obj2, false);
			break;
		case 1:
			turnOn (obj, false);
			turnOn (obj1, true);
			turnOn (obj2, false);
			break;
		case 2:
			turnOn (obj, true);
			turnOn (obj1, true);
			turnOn (obj2, false);
			break;
		case 3:
			turnOn (obj, true);
			turnOn (obj1, true);
			turnOn (obj2, true);
			break;
		}
		//turnOn(Random.Range(1,5)!=1);

        if(numLights > lastState) {
            AkSoundEngine.PostEvent("Green_Light", ButtonBehaviors.musicHolder);
            lastState = numLights;
        }
	}

	public void turnOn(GameObject obj, bool val) {
		obj.SetActive (val);
	}
}
