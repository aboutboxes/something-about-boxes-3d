﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreController {

    //Win Condition is an array that keeps the set of 3 that will result in cookie giving.
    private int[] winCondition = new int[3] { 0, 1, 2 };
    private int level = 1;

    //Text Labels
    private Text LightLabel_1; // GameObject.Find("LightLabel_1").GetComponent<Text>();
    private Text LightLabel_2; // GameObject.Find("LightLabel_2").GetComponent<Text>();
    private Text LightLabel_3; // GameObject.Find("LightLabel_3").GetComponent<Text>();
    private string[] labels = new string[3] { "Test", "Test", "Test"};

    //Constructor for Scorecontroller
    public ScoreController()
    {
        LightLabel_1 = GameObject.Find("LightLabel_1").GetComponent<Text>();
        LightLabel_2 = GameObject.Find("LightLabel_2").GetComponent<Text>();
        LightLabel_3 = GameObject.Find("LightLabel_3").GetComponent<Text>();

        //Creates a new Win Condition on startup
        createWinCondition();

        //Creates Light Labels for the Win Condition
        set_LightLabels();
    }

    //This is a function used to reset the properties of scorecontroller
    public void ReInitScore()
    {
        //Creates a new Win Condition on when needed
        createWinCondition();
        //Creates Light Labels for the Win Condition
        set_LightLabels();
    }

    public void set_LightLabels(){
        //Iterate through the labels and the winCondition Array to assign a text value
        for (int i = 0; i < 3; i++){
            switch (winCondition[i]){
                case 0:
                    labels[i] = "RUN";
                    break;
                case 1:
                    labels[i] = "ROLL";
                    break;
                case 2:
                    labels[i] = "BUTTON";
                    break;
                case 3:
                    labels[i] = "DANCE";
                    break;
                case 4:
                    labels[i] = "LAUGH";
                    break;
                case 5:
                    labels[i] = "LAY DOWN";
                    break;
            }
        }

        LightLabel_1.text = labels[0];
        LightLabel_2.text = labels[1];
        LightLabel_3.text = labels[2];
    }

    //This function will be used to determine a win condition when a creature is created.
    public void createWinCondition() {
         for(int i = 0; i < 3; i++){
            winCondition[i] = Random.Range(0, 3); //Change this depending on how many actions we want
         }
        set_LightLabels();
    }

    //Getter and Setter for lvl
    public void setLevel(int i) { level = i;}
    public int getLevel() { return level; }


    //This will check to see how many lights the monster should turn on potentially. 
    public int checkLights() {
		if (winCondition [0] == ButtonBehaviors.controller.memory.Get (2) && winCondition [1] == ButtonBehaviors.controller.memory.Get (1)
		    && winCondition [2] == ButtonBehaviors.controller.memory.Get (0) && level == 3) {
			return 3;
		}
		if (winCondition [0] == ButtonBehaviors.controller.memory.Get (1) && winCondition [1] == ButtonBehaviors.controller.memory.Get (0) && level >= 2) {
			return 2;
		}
		if (winCondition [0] == ButtonBehaviors.controller.memory.Get (0) && level >= 1) {
			return 1;
		}
		return 0;
    }

}
