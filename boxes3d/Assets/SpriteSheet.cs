﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using SimpleJson;
using System;

public class SpriteSheet : MonoBehaviour {

	public TextAsset jsonText;
	private Renderer rend;
	private FlashAnimationInfo animationInfo;
	public int[] frameCycle = new int[]{};
	private int cycleIndex = -1;
	private int farclose = -1;
	public float lastUpdate;
	public float minPeriod = 3.0f;
	public int currentBehavior;
	public bool oneTime = false;
	public float timeToStill = 0;
	public float speed = 100f;
	public GameObject cube;
	private float endPunishment = 0;
	public int offsetFrame = 0;

	public Vector3 goal;
	public Vector3 startPosition;

	public Transform buttonPosition;
	public bool Landing {
		get {
			TimeSpan time = DateTime.Now.TimeOfDay;
			float now = (float)time.TotalSeconds;
			return now - landingTime < 3;
		}
	}
	private float landingTime;

	public enum Animations {
		NONE,
		STILL,
		RUN,
		EAT,
		LAUGH,
		ROLL,
		LIEDOWN,
		DANCE,
		LAND,
		LIEDOWNSAD,
		HURT
	};

	Animations oldAnim = Animations.NONE;
	private Text logArea;

	// Use this for initialization
	void Start () {
		startPosition = transform.position;
		rend = GetComponent<Renderer>();
		animationInfo = JsonUtility.FromJson<FlashAnimationInfo> (jsonText.text);
		Appear ();
	}

	public void Appear() {
//		transform.position = startPosition;
		changeAnimation (Animations.LAND);
		goal = transform.position;
		TimeSpan time = DateTime.Now.TimeOfDay;
		float now = (float)time.TotalSeconds;
		landingTime = now;
		oneTime = true;
		cycleIndex = -1;
		offsetFrame = (int)(Time.time * 24.0f);
		gameObject.SetActive (true);
		logArea = GameObject.Find ("LogArea").GetComponent<Text>();
	}

	Animations getMapping(int anim) {
		switch (anim) {
		case 0:
			return Animations.RUN;
		case 1:
			return Animations.ROLL;
		case 2:
			return Animations.RUN;
		case 3:
			return Animations.DANCE;
		case 4:
			return Animations.LAUGH;
		case 5:
			return Animations.LIEDOWN;
		default:
			return Animations.STILL;
		}
	}

    string getSoundName(int anim){
        switch (anim)
        {
            case 0:
                return "Creature_Walk";
            case 1:
                return "Creature_Roll";
            case 2:
                return "Creature_Walk";
            case 3:
                return "Creature_Dance";
            case 4:
                return "Creature_Laugh";
            case 5:
                return "Creature_Lie_Down";
            default:
                return "Creature_Idle";
        }

    }

	void Behave(int behavior) {
		speed = UnityEngine.Random.Range (100f, 500f);
		//Debug.Log (">>>>" + behavior);
		switch (behavior) {
		case 0: 	//	 run
		case 1:		//	ROLL
			goal = new Vector3 (UnityEngine.Random.Range (-113f, 559f), startPosition.y, UnityEngine.Random.Range (0f, 291f));
			oneTime = false;
			break;
		case 2:	//	BUTTON
			goal = new Vector3(buttonPosition.position.x,startPosition.y,buttonPosition.position.z);
			oneTime = false;
			speed = 500f;
			break;
		case 5:
			oneTime = true;
			goal = transform.position;
			break;
		default:
			goal = transform.position;
			oneTime = false;
			break;
		}
		//Debug.Log (transform.position + ", " + goal);
		Animations anim = getMapping (behavior);
		changeAnimation (anim);
        AkSoundEngine.PostEvent(getSoundName(behavior), ButtonBehaviors.musicHolder);

		Vector3 direction = goal - transform.position;
		if (direction.x * transform.localScale.x < 0) {
			transform.localScale = new Vector3 (-transform.localScale.x, transform.localScale.y, transform.localScale.z);
		}



	}

	public bool gettingPunished() {
		return frameCycle [cycleIndex % frameCycle.Length] == 163;
	}

	void StandStill() {
		switch (oldAnim) {
		case Animations.ROLL:
		case Animations.RUN:
			changeAnimation (Animations.STILL);
                AkSoundEngine.PostEvent("Creature_Idle", ButtonBehaviors.musicHolder);
			break;
		}
		goal = transform.position;
	}

	GameObject ClosestCookie() {
		GameObject[] objs = GameObject.FindGameObjectsWithTag ("cookie");
		GameObject closest = null;
		float mindist = float.MaxValue;
		foreach (GameObject cookie in objs) {
			float dist = Vector3.Distance (transform.position, cookie.transform.position);
			if (dist < mindist) {
				closest = cookie;
				mindist = dist;
			}
		}
		if (closest != null) {
			return closest;
		}
		return null;
	}

	public void punishMyself() {
		if (gameObject.activeSelf == false) {
			return;
		}
        Debug.Log ("HERE");
        cube.transform.position = new Vector3(transform.position.x, 500f, transform.position.z);
		Vector3 dir = transform.position - cube.transform.position;
		dir.Normalize();
		cube.GetComponent<Rigidbody> ().velocity = (dir * 4000.0f);
		cube.GetComponent<Rigidbody> ().angularVelocity = UnityEngine.Random.insideUnitSphere * 1000f;
		changeAnimation (Animations.HURT);
        AkSoundEngine.PostEvent("Creature_Sad_Idle", ButtonBehaviors.musicHolder);
		TimeSpan time = DateTime.Now.TimeOfDay;
		float now = (float)time.TotalSeconds;
		endPunishment = now + 3.0f;
		oneTime = true;
		cycleIndex = -1;
		offsetFrame = (int)(Time.time * 24.0f);
		goal = transform.position;

		lastUpdate += 1f;
	}

	private bool dontMove() {
		return frameCycle [cycleIndex % frameCycle.Length] >= 61 && frameCycle [cycleIndex % frameCycle.Length] <= 69;
	}

	// Update is called once per frame
	void Update () {
		TimeSpan time = DateTime.Now.TimeOfDay;
		float now = (float)time.TotalSeconds;

		logArea.text = ButtonBehaviors.controller.DumpState ();
		Vector3 destination = goal;

		bool gettingPunished = (now < endPunishment);
		GameObject cookie = ClosestCookie ();
		if (cookie != null && !gettingPunished && !Landing) {
			destination = new Vector3(cookie.transform.position.x,startPosition.y,cookie.transform.position.z);
			Vector3 dir = destination - transform.position;
			if (dir.x * transform.localScale.x < 0) {
				transform.localScale = new Vector3 (-transform.localScale.x, transform.localScale.y, transform.localScale.z);
			}
		}

		Vector3 direction = destination - transform.position;
		//Debug.Log ("direction >> " + direction);


		float distance = direction.magnitude;
		if (distance > 1) {
			oneTime = false;
			switch (oldAnim) {
			case Animations.ROLL:
			case Animations.RUN:
				break;
			default:
				changeAnimation (Animations.RUN);
				break;
			}
			direction.Normalize ();
			if (!dontMove ()) {
				transform.position += direction * Time.deltaTime * speed;
			}
		} else {
			StandStill ();
			if (cookie != null) {
				GameObject.Destroy (cookie);
//				oneTime = true;
				changeAnimation (Animations.EAT);
                AkSoundEngine.PostEvent("Creature_Eating", ButtonBehaviors.musicHolder);
				lastUpdate += 1f;
			}
		}


		if (now < endPunishment) {

		}
		else if (now > timeToStill) {
			StandStill ();
		}

		int cycle = (oneTime ? Math.Min((int)(Time.time * 24.0f - offsetFrame), frameCycle.Length-1) : (int)(Time.time * 24.0f - offsetFrame) % frameCycle.Length);
		if (cycle != cycleIndex) {
			cycleIndex = cycle;
			int frameIndex = frameCycle [cycleIndex];
			Debug.Log (oldAnim + " => " + cycleIndex + " => " + frameIndex);

			Frame frame = animationInfo.frames [frameIndex];
			Size totalSize = animationInfo.meta.size;
//			totalSize.w /= 2; totalSize.h /= 2;
			this.rend.material.mainTextureScale = new Vector2 (frame.sourceSize.w / totalSize.w, frame.sourceSize.h / totalSize.h);
			this.rend.material.mainTextureOffset = new Vector2 (frame.frame.x / totalSize.w, (totalSize.h - (frame.frame.y + frame.sourceSize.h)) / totalSize.h);
		}
		if (now - lastUpdate >= minPeriod && !Landing && !gettingPunished) {
			lastUpdate = now;
			currentBehavior = ButtonBehaviors.controller.NextBehaviorSeq ();
			Behave (currentBehavior);
			timeToStill = now + minPeriod - 1.0f;
//			Animations anim = getMapping (currentBehavior);
//			Debug.Log (anim);
//			changeAnimation (anim);
		}
		/*if (Input.GetKeyDown (KeyCode.D)) {
			changeAnimation (Animations.DANCE);
		}
		if (Input.GetKeyDown (KeyCode.E)) {
			changeAnimation (Animations.EAT);
		}
		if (Input.GetKeyDown (KeyCode.I)) {
			changeAnimation (Animations.HURT);
		}
		if (Input.GetKeyDown (KeyCode.L)) {
			changeAnimation (Animations.LAND);
		}
		if (Input.GetKeyDown (KeyCode.H)) {
			changeAnimation (Animations.LAUGH);
		}
		if (Input.GetKeyDown (KeyCode.J)) {
			changeAnimation (Animations.LIEDOWN);
		}
		if (Input.GetKeyDown (KeyCode.K)) {
			changeAnimation (Animations.LIEDOWNSAD);
		}
		if (Input.GetKeyDown (KeyCode.O)) {
			changeAnimation (Animations.ROLL);
		}
		if (Input.GetKeyDown (KeyCode.P)) {
			changeAnimation (Animations.RUN);
		}
		if (Input.GetKeyDown (KeyCode.U)) {
			changeAnimation (Animations.STILL);
		}*/
	}

    public void changeAnimation(Animations anim) {
		//Debug.Log (anim);
		if (anim == oldAnim) {
			return;
		}
		oldAnim = anim;
		int startFrame = 0, endFrame = 0;
		switch (anim) {
		case Animations.DANCE:
			startFrame = 176;
			endFrame = 216;
			break;
		case Animations.EAT:
			startFrame = 70;
			endFrame = 111;
			break;
		case Animations.HURT:
			startFrame = 260;
			endFrame = 281;
			break;
		case Animations.LAND:
			startFrame = 217;
			endFrame = 239;
			break;
		case Animations.LAUGH:
			startFrame = 112;
			endFrame = 143;
			break;
		case Animations.LIEDOWN:
			startFrame = 156;
			endFrame = 175;
			break;
		case Animations.LIEDOWNSAD:
			startFrame = 240;
			endFrame = 259;
			break;
		case Animations.ROLL:
			startFrame = 144;
			endFrame = 155;
			break;
		case Animations.RUN:
			startFrame = 50;
			endFrame = 69;
			break;
		case Animations.STILL:
			startFrame = 1;
			endFrame = 49;
			break;
		}

		int[] frames = new int[endFrame - startFrame];
		for (int i = 0; i < frames.Length; ++i) {
			frames [i] = startFrame + i;
		}
		frameCycle = frames;
		cycleIndex = 0;
		offsetFrame = (int)(Time.time * 24.0f);
	}


}
