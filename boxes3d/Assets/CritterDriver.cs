﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class CritterDriver : MonoBehaviour {

	public float lastUpdate;
	public float minPeriod = 1.0f;

	// Use this for initialization
	void Start () {
/*		
		CircularBuffer<int> c = new CircularBuffer<int>(4);
		c.Put(5); c.Put(6);
		Debug.Log(c.ToString());
		c.Put(7); c.Put (8); c.Put(9); c.Put(10); c.Put(11); c.Put(12);
		Debug.Log(c.ToString());
		Debug.Log("get " + c.Get() + " get2 " + c.Get(2));
*/
	}
	
	// Update is called once per frame
	void Update () {
		TimeSpan time = DateTime.Now.TimeOfDay;
		float now = (float)time.TotalSeconds;
		
		if(now - lastUpdate >= minPeriod) {
			lastUpdate = now;
			//int next = ButtonBehaviors.controller.NextBehavior();
			//Debug.Log("next " + next);
		}
	}
}
