﻿using UnityEngine;
using System.Collections.Generic;
using System;

[Serializable]
public struct Size {
	public float w;
	public float h;
}

[Serializable]
public struct Rectangle {
	public float x;
	public float y;
	public float w;
	public float h;
}

[Serializable]
public struct Frame {
	public string filename;
	public Rectangle frame;
	public bool rotated;
	public bool trimmed;
	public Rectangle spriteSourceSize;
	public Size sourceSize;
}

[Serializable]
public struct Meta {
	public string app;
	public string version;
	public string image;
	public string format;
	public Size size;
	public float scale;
}

public class FlashAnimationInfo {
	public Frame[] frames;
	public Meta meta;

	public FlashAnimationInfo() {
		frames = new Frame[3];
	}
}