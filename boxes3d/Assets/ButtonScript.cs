﻿using UnityEngine;
using System.Collections;
using System;

public class ButtonScript : MonoBehaviour {

	public UnityEngine.GameObject button;
	public UnityEngine.GameObject[] critters;

    private bool pushable = true;
	Boolean buttonDown = false;
	// Use this for initialization
	void Start () {

	}

    private IEnumerator Cooldown()
    {
       pushable = false;
       yield return new WaitForSeconds(2.5f); // waits 3 seconds
       pushable = true; // will make the update method pick up 
    }

    // Update is called once per frame
    void Update () {
		Vector3 buttonTransform = button.transform.position;
        int onButton = 0;
        foreach(GameObject critter in critters) {
            if (!critter.activeSelf) {
                continue;
            }
    		Vector3 critterTransform = critter.transform.position;
    		//Debug.Log ("Distance" + Vector3.Distance (buttonTransform, critterTransform));
    		if (Vector3.Distance (buttonTransform, critterTransform) < 200) {
                onButton ++;
            }
        }
        if(onButton>0) {
            if (!buttonDown && pushable) {
                buttonDown = true;
                StartCoroutine(Cooldown());
                AkSoundEngine.PostEvent("Creature_Press_Button", ButtonBehaviors.musicHolder);
                button.transform.position = new Vector3 (buttonTransform.x, -85, buttonTransform.z);
            } else if (buttonDown && pushable) {
                button.transform.position = new Vector3 (buttonTransform.x, -73, buttonTransform.z);
                buttonDown = false;
            }
        }
	}
}
