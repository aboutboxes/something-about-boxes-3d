﻿using UnityEngine;
using System.Collections;

public class PlayMusic : MonoBehaviour {
    GameObject holder;
	// Use this for initialization
	void Start () {
        holder = GameObject.Find("MusicHolder");
        AkSoundEngine.PostEvent("Play_Music", this.gameObject);

    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
