﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
//using System;

public class CritterController {
	
	//private float lastUpdate;
	//private float minPeriod = 1.0f;
	public int numBehaviors = 3;
	public float initialWeight = 2.0f;
	public float adjustWeight = 0.5f;
	//public CritterBehavior[] behaviors;
	public float[] behaviorWeights;

	private float minWeight = 1.0f; // no matter how much punishment, min
	private float maxWeight = 4.0f; // no matter how much reward, max

	public float mutationOdds = 0.1f; //odds of random behavior change

	public CircularBuffer<int> memory;
	private Text logArea;
	public CircularBuffer<int> sequence;
	public int sequenceLength = 3;

	//private int[] lastBehaviors;
	//private int lastBehaviorPtr = 0;
	private int memLength = 10;
	// private bool test = true;

	// Use this for initialization
	public CritterController (int behaviors = 3, int sequences = 3) {

		Reinit(behaviors, sequences);

		//memory = new CircularBuffer<int>(memLength);
		//lastBehaviors = new int[memLength];
		//for(int i = 0; i < memLength; ++i) {
		//	lastBehaviors[i] = i % numBehaviors;
		//}

		logArea = GameObject.Find("LogArea").GetComponent<Text>();

	}

	public void Reinit(int behaviors = 3, int sequences = 3) {
		numBehaviors = behaviors;
		sequenceLength = sequences;

		behaviorWeights = new float[numBehaviors];
		for(int i = 0; i < numBehaviors; ++i) {
			behaviorWeights[i] = initialWeight;
		}

		memory = new CircularBuffer<int>(memLength);
		for (int i = 0; i < memLength; ++i) {
			memory.Put(-1);
		}
		sequence = new CircularBuffer<int>(sequenceLength);
		for (int i = 0; i < sequenceLength; ++i) {
			sequence.Put(UnityEngine.Random.Range(0, numBehaviors));
		}
	}

	public void Reward(int amount = 1) {
        if (memory.Get() < 0) {
            return;
        }
		if (amount < 0){ 
			float rewardSize = -0.5f * adjustWeight;
			behaviorWeights[memory.Get()] = Mathf.Max(
				minWeight,
				behaviorWeights[memory.Get()] + (rewardSize * behaviorWeights[memory.Get()])
			);
			ReplaceLastBehavior();
		} else {
			behaviorWeights[memory.Get()] = Mathf.Min(
				maxWeight,
				behaviorWeights[memory.Get()] + (adjustWeight * behaviorWeights[memory.Get()])
			);
		}

		Debug.Log("fed when last action was " + memory.Get());

		/*
		behaviorWeights[memory.Get(1)] +=
			(rewardSize * 0.25f * behaviorWeights[memory.Get(1)]);

		behaviorWeights[memory.Get(2)] +=
			(rewardSize * 0.0625f * behaviorWeights[memory.Get(2)]);
		
		logArea.text = joinFloats(behaviorWeights) + "\nfed " +
			memory.Get(1) + " " + memory.Get(2) + " " + memory.Get(3) + "\nseq " +
			sequence.Dump();
		*/


		/* 
		behaviorWeights[lastBehaviors[lastBehaviorPtr]] += 
			(rewardSize * behaviorWeights[lastBehaviors[lastBehaviorPtr]]);

		int prev = lastBehaviorPtr >= 1 ? lastBehaviorPtr - 1 : memLength - 1;
		behaviorWeights[lastBehaviors[prev]] += 
			(rewardSize * 0.25f * behaviorWeights[lastBehaviors[prev]]);

		int prevprev = prev >= 1 ? prev - 1: memLength - 1;
		behaviorWeights[lastBehaviors[prevprev]] += 
			(rewardSize * 0.0625f * behaviorWeights[lastBehaviors[prevprev]]);

		logArea.text = joinFloats(behaviorWeights) + "\nfed " + lastBehaviors[lastBehaviorPtr] + 
			" " + lastBehaviors[prev] + " " + lastBehaviors[prevprev] + " weight " + amount;
		*/
	}

	private int WeightedRoll () {
		// weighted dice roll
		float sumWeight = 0.0f;

		for (int i = 0; i < numBehaviors; ++i) {
			sumWeight += behaviorWeights[i];
		}

		int next = 0;
		float r = UnityEngine.Random.Range(0.0f, sumWeight);

		for(int i = 0; i < numBehaviors; i++) {
			r -= behaviorWeights[i];
			if(r<=0) {
				next = i;
				break;
			}
		}
		return next;
	}

	public int NextBehavior() {
		int next = 0;

		// weighted dice roll
		next = WeightedRoll();
			
		// remember what we did
		memory.Put(next);
//		lastBehaviorPtr = (lastBehaviorPtr + 1) % memLength;
//		lastBehaviors[lastBehaviorPtr] = next;

		return next;
	}

	public int NextBehaviorSeq() {
		int next = sequence.GetNext();

		if(Random.value < mutationOdds) {
			// ReplaceLastBehavior("mutated");
		}
		memory.Put(next);

		return next;
	}

	private void ReplaceLastBehavior(string reason = "replaced ") {
		int next = sequence.Get(1);
		string ax = reason + next;
		next = WeightedRoll();
		ax += " -> " + next;
		Debug.Log(ax);
		sequence.Replace(next, 1);
	}

	private string joinInts (int[] ints) {
		string ax = "";
		for(int i = 0; i < ints.Length; i++) {
			ax += ints[i] + " ";
		}
		return ax;
	}

	private string joinFloats (float[] fs) {
		string ax = "";
		for(int i = 0; i < fs.Length; i++) {
			ax += fs[i].ToString("n2") + " ";
		}
		return ax;
	}

	public string DumpState() {
		string ax = joinFloats(behaviorWeights) + "\nfed " +
			memory.Get(1) + " " + memory.Get(2) + " " + memory.Get(3) + "\nseq ";

		ax += sequence.Dump().Replace("0", "run").Replace("1", "roll").Replace("2", "button").
		Replace("3", "dance").Replace("4", "laugh").Replace("5", "down");

		return ax;
	}

}
