﻿using UnityEngine;
using System.Collections;

public class CookieThrower : MonoBehaviour {

	public GameObject cookiePrefab;
	public GameObject playerCookiePrefab;
	public GameObject specialTreatPrefab;

	public void ThrowCookie(int type) {
		GameObject cookie = GameObject.Instantiate (type==0?cookiePrefab:type==1?playerCookiePrefab:specialTreatPrefab);
		cookie.transform.position = transform.position;
		cookie.GetComponent<Rigidbody>().AddForce (new Vector3 (-50f, +50, -50f), ForceMode.Impulse);
 	}
}
