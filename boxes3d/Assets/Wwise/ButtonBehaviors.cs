﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class ButtonBehaviors : MonoBehaviour {

	public static CritterController controller;
	public float lastUpdate;
	public float minPeriod = 1.0f;
	public static ScoreController scoreController;
	public SpriteSheet[] spritesheets;

	private Button feedButton;
	private Button punishButton;
	private Button dropButton;
	private float buttonCooldownTime = 0.75f;

    public static GameObject musicHolder;

	// Use this for initialization
	void Start () {
		controller = new CritterController(6,3);
		scoreController = new ScoreController();

		feedButton = GameObject.Find("Feed").GetComponent<Button>();
		punishButton = GameObject.Find("Punish").GetComponent<Button>();
		dropButton = GameObject.Find("Drop").GetComponent<Button>();

        musicHolder = GameObject.Find("MusicHolder");

	}
	
	// Update is called once per frame
	void Update () {
		TimeSpan time = DateTime.Now.TimeOfDay;
		float now = (float)time.TotalSeconds;

		if(Mathf.Abs(now - lastUpdate) >= minPeriod) {
			lastUpdate = now;
			// put the update logic here
		}
	}

	public void Feed() {
        AkSoundEngine.PostEvent("Creature_Food_Chute", gameObject);
        controller.Reward(1);
		StartCoroutine(Cooldown(feedButton));

		Debug.Log("Feed");
	}

	public IEnumerator Cooldown(Button b) {
		b.interactable = false;
		yield return new WaitForSeconds(buttonCooldownTime); // waits 3 seconds
		b.interactable = true; // will make the update method pick up 
	}

	public void Punish() {
		
		controller.Reward(-1);
		Debug.Log("Punish");
        AkSoundEngine.PostEvent("Creature_Fall", ButtonBehaviors.musicHolder);
        StartCoroutine(Cooldown(punishButton));
		for (int i = 0; i < spritesheets.Length; ++i) {
			spritesheets [i].punishMyself ();
		}
	}

	public void Drop() {
		StartCoroutine(Cooldown(dropButton));
		Debug.Log("Drop");
        AkSoundEngine.PostEvent("Creature_Dump", ButtonBehaviors.musicHolder);
        SpriteSheet[] sprites = PlayerManager.Instance.sprites;
		int n = 0;
		for (int i = 0; i < sprites.Length; i++) {
			if (sprites [i].gameObject.activeSelf) {
				sprites [i].gameObject.SetActive (false);
				n = i;
				break;
			}
		}
		sprites [(n+1)%sprites.Length].Appear ();

		controller.Reinit (6,3);
        scoreController.ReInitScore();

        GameObject hunger = GameObject.FindGameObjectWithTag ("hunger");
		hunger.GetComponent<Hunger> ().Initialize();
	}
}
