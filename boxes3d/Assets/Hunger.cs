﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Hunger : MonoBehaviour {

	public Slider slider;
	public CookieThrower thrower;
    private float decrement = 0.00025f;
    private float increment = 0.002f;

    // Use this for initialization
    void Start () {

    }

	public void Initialize() {
		slider.value = .5f;
	}

    // Update is called once per frame
    // Update is called once per frame
    void Update(){
        slider.value -= decrement;
        int level = ButtonBehaviors.scoreController.getLevel();
        if (ButtonBehaviors.scoreController.checkLights() == level){
            slider.value += increment;
            if (slider.value >= 1.0f){
                thrower.ThrowCookie(1);
                AkSoundEngine.PostEvent("Player_Food_Chute", ButtonBehaviors.musicHolder);
                ButtonBehaviors.scoreController.setLevel(++level);
                if (level > 3){
                    decrement = 0.0f;
                    Debug.Log("YOU WIN!");
                }
                else {
                    slider.value = 0.5f;
                    if (level == 2){
                        decrement = 0.0002f;
                    }
                    else if (level == 3){
                        decrement = 0.00005f;
                        increment = 0.0035f;
                    }
                }
            }
            //slider.value += increment;
        }
        else if (slider.value == 0.0f){
            Debug.Log("YOU LOSE");
        }
    }
}
