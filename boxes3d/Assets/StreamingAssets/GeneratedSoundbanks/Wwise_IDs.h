/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID CREATURE_BURP = 2450242320U;
        static const AkUniqueID CREATURE_DANCE = 1812180666U;
        static const AkUniqueID CREATURE_DROP = 1019326462U;
        static const AkUniqueID CREATURE_DUMP = 984638263U;
        static const AkUniqueID CREATURE_EATING = 917153547U;
        static const AkUniqueID CREATURE_EXCITED = 140563081U;
        static const AkUniqueID CREATURE_FALL = 1694601326U;
        static const AkUniqueID CREATURE_FOOD_CHUTE = 2062654587U;
        static const AkUniqueID CREATURE_IDLE = 1671389507U;
        static const AkUniqueID CREATURE_LAUGH = 1477662452U;
        static const AkUniqueID CREATURE_LIE_DOWN = 3910625512U;
        static const AkUniqueID CREATURE_LIE_DOWN_SAD = 957534941U;
        static const AkUniqueID CREATURE_PAIN = 1375780685U;
        static const AkUniqueID CREATURE_PRESS_BUTTON = 1374171797U;
        static const AkUniqueID CREATURE_ROLL = 1542165972U;
        static const AkUniqueID CREATURE_SAD_IDLE = 728707208U;
        static const AkUniqueID CREATURE_WALK = 2817266618U;
        static const AkUniqueID ELECTRIC_SHOCK = 2741180647U;
        static const AkUniqueID GREEN_LIGHT = 1990338295U;
        static const AkUniqueID PLAY_MUSIC = 2932040671U;
        static const AkUniqueID PLAYER_EATING = 314804603U;
        static const AkUniqueID PLAYER_FOOD_CHUTE = 3391349163U;
        static const AkUniqueID RED_LIGHT = 385957527U;
    } // namespace EVENTS

    namespace GAME_PARAMETERS
    {
        static const AkUniqueID SS_AIR_FEAR = 1351367891U;
        static const AkUniqueID SS_AIR_FREEFALL = 3002758120U;
        static const AkUniqueID SS_AIR_FURY = 1029930033U;
        static const AkUniqueID SS_AIR_MONTH = 2648548617U;
        static const AkUniqueID SS_AIR_PRESENCE = 3847924954U;
        static const AkUniqueID SS_AIR_RPM = 822163944U;
        static const AkUniqueID SS_AIR_SIZE = 3074696722U;
        static const AkUniqueID SS_AIR_STORM = 3715662592U;
        static const AkUniqueID SS_AIR_TIMEOFDAY = 3203397129U;
        static const AkUniqueID SS_AIR_TURBULENCE = 4160247818U;
    } // namespace GAME_PARAMETERS

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID FEEDME_SOUNDBANK = 539836021U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
        static const AkUniqueID MASTER_SECONDARY_BUS = 805203703U;
    } // namespace BUSSES

}// namespace AK

#endif // __WWISE_IDS_H__
