Event	ID	Name			Wwise Object Path	Notes
	140563081	Creature_Excited			\Default Work Unit\Creature_Excited	
	314804603	Player_Eating			\Default Work Unit\Player_Eating	
	385957527	Red_Light			\Default Work Unit\Red_Light	
	728707208	Creature_Sad_Idle			\Default Work Unit\Creature_Sad_Idle	
	917153547	Creature_Eating			\Default Work Unit\Creature_Eating	
	957534941	Creature_Lie_Down_Sad			\Default Work Unit\Creature_Lie_Down_Sad	
	984638263	Creature_Dump			\Default Work Unit\Creature_Dump	
	1019326462	Creature_Drop			\Default Work Unit\Creature_Drop	
	1374171797	Creature_Press_Button			\Default Work Unit\Creature_Press_Button	
	1375780685	Creature_Pain			\Default Work Unit\Creature_Pain	
	1477662452	Creature_Laugh			\Default Work Unit\Creature_Laugh	
	1542165972	Creature_Roll			\Default Work Unit\Creature_Roll	
	1671389507	Creature_Idle			\Default Work Unit\Creature_Idle	
	1694601326	Creature_Fall			\Default Work Unit\Creature_Fall	
	1812180666	Creature_Dance			\Default Work Unit\Creature_Dance	
	1990338295	Green_Light			\Default Work Unit\Green_Light	
	2062654587	Creature_Food_Chute			\Default Work Unit\Creature_Food_Chute	
	2450242320	Creature_Burp			\Default Work Unit\Creature_Burp	
	2741180647	Electric_Shock			\Default Work Unit\Electric_Shock	
	2817266618	Creature_Walk			\Default Work Unit\Creature_Walk	
	2932040671	Play_Music			\Default Work Unit\Play_Music	
	3391349163	Player_Food_Chute			\Default Work Unit\Player_Food_Chute	
	3910625512	Creature_Lie_Down			\Default Work Unit\Creature_Lie_Down	

In Memory Audio	ID	Name	Audio source file		Wwise Object Path	Notes	Data Size
	9624793	Creature Sad Idle_02	C:\Users\seth\Desktop\Wwise\FeedMe\.cache\Windows\SFX\Creature Sad Idle_02_E843A46D.wem		\Actor-Mixer Hierarchy\Default Work Unit\Creature_Sad_Idle\Creature Sad Idle_02		17191
	38055460	Creature Sad Idle_07	C:\Users\seth\Desktop\Wwise\FeedMe\.cache\Windows\SFX\Creature Sad Idle_07_E843A46D.wem		\Actor-Mixer Hierarchy\Default Work Unit\Creature_Sad_Idle\Creature Sad Idle_07		17892
	84211258	Creature Roll_02	C:\Users\seth\Desktop\Wwise\FeedMe\.cache\Windows\SFX\Creature Roll_02_E843A46D.wem		\Actor-Mixer Hierarchy\Default Work Unit\Creature_Roll\Creature Roll_02		7315
	103817555	Creature Pain_03	C:\Users\seth\Desktop\Wwise\FeedMe\.cache\Windows\SFX\Creature Pain_03_E843A46D.wem		\Actor-Mixer Hierarchy\Default Work Unit\Creature_Pain\Creature Pain_03		8592
	139198385	Creature Excited_02	C:\Users\seth\Desktop\Wwise\FeedMe\.cache\Windows\SFX\Creature Excited_02_E843A46D.wem		\Actor-Mixer Hierarchy\Default Work Unit\Creature_Excited\Creature Excited_02		24187
	144911698	Player_Food_Chute_01	C:\Users\seth\Desktop\Wwise\FeedMe\.cache\Windows\SFX\Food Chute Player_01_E843A46D.wem		\Actor-Mixer Hierarchy\Default Work Unit\Player_Food_Chute_01		17563
	149059704	Creature_Food_Chute_01	C:\Users\seth\Desktop\Wwise\FeedMe\.cache\Windows\SFX\Food Chute Creature_01_E843A46D.wem		\Actor-Mixer Hierarchy\Default Work Unit\Creature_Food_Chute_01		14176
	154888488	Creature Idle_01	C:\Users\seth\Desktop\Wwise\FeedMe\.cache\Windows\SFX\Creature Idle_01_E843A46D.wem		\Actor-Mixer Hierarchy\Default Work Unit\Creature_Idle\Creature Idle_01		8557
	161151653	Creature Lie Down Sad_04	C:\Users\seth\Desktop\Wwise\FeedMe\.cache\Windows\SFX\Creature Lie Down Sad_04_E843A46D.wem		\Actor-Mixer Hierarchy\Default Work Unit\Creature_Lie_Down_Sad\Creature Lie Down Sad_04		5065
	161801547	Creature Dance_01	C:\Users\seth\Desktop\Wwise\FeedMe\.cache\Windows\SFX\Creature Dance_01_E843A46D.wem		\Actor-Mixer Hierarchy\Default Work Unit\Creature_Dance\Creature Dance_01		20689
	173759764	Creature Burp_01	C:\Users\seth\Desktop\Wwise\FeedMe\.cache\Windows\SFX\Creature Burp_01_E843A46D.wem		\Actor-Mixer Hierarchy\Default Work Unit\Creature Burp_01		2788
	192976471	Creature Lie Down_01	C:\Users\seth\Desktop\Wwise\FeedMe\.cache\Windows\SFX\Creature Lie Down_01_E843A46D.wem		\Actor-Mixer Hierarchy\Default Work Unit\Creature_Lie_Down\Creature Lie Down_01		5065
	261230998	Creature Idle_04	C:\Users\seth\Desktop\Wwise\FeedMe\.cache\Windows\SFX\Creature Idle_04_E843A46D.wem		\Actor-Mixer Hierarchy\Default Work Unit\Creature_Idle\Creature Idle_04		11073
	268895706	Creature Eating_02	C:\Users\seth\Desktop\Wwise\FeedMe\.cache\Windows\SFX\Creature Eating_02_E843A46D.wem		\Actor-Mixer Hierarchy\Default Work Unit\Creature_Eating\Creature Eating_02		17524
	271322325	Green Light_01	C:\Users\seth\Desktop\Wwise\FeedMe\.cache\Windows\SFX\Green Light_01_E843A46D.wem		\Actor-Mixer Hierarchy\Default Work Unit\Green Light_01		15246
	288828073	Player Eating_01	C:\Users\seth\Desktop\Wwise\FeedMe\.cache\Windows\SFX\Player Eating_01_E843A46D.wem		\Actor-Mixer Hierarchy\Default Work Unit\Player_Eating\Player Eating_01		23578
	301540822	Creature Pain_02	C:\Users\seth\Desktop\Wwise\FeedMe\.cache\Windows\SFX\Creature Pain_02_E843A46D.wem		\Actor-Mixer Hierarchy\Default Work Unit\Creature_Pain\Creature Pain_02		7655
	317453723	Creature Idle_03	C:\Users\seth\Desktop\Wwise\FeedMe\.cache\Windows\SFX\Creature Idle_03_E843A46D.wem		\Actor-Mixer Hierarchy\Default Work Unit\Creature_Idle\Creature Idle_03		5936
	338020923	Player Eating_02	C:\Users\seth\Desktop\Wwise\FeedMe\.cache\Windows\SFX\Player Eating_02_E843A46D.wem		\Actor-Mixer Hierarchy\Default Work Unit\Player_Eating\Player Eating_02		19565
	356961396	Creature Excited_01	C:\Users\seth\Desktop\Wwise\FeedMe\.cache\Windows\SFX\Creature Excited_01_E843A46D.wem		\Actor-Mixer Hierarchy\Default Work Unit\Creature_Excited\Creature Excited_01		22451
	363076363	Creature Lie Down Vox_01	C:\Users\seth\Desktop\Wwise\FeedMe\.cache\Windows\SFX\Creature Lie Down Vox_01_E843A46D.wem		\Actor-Mixer Hierarchy\Default Work Unit\Creature_Lie_Down\Creature Lie Down Vox_01		7980
	371475890	Creature Idle_07	C:\Users\seth\Desktop\Wwise\FeedMe\.cache\Windows\SFX\Creature Idle_07_E843A46D.wem		\Actor-Mixer Hierarchy\Default Work Unit\Creature_Idle\Creature Idle_07		14836
	418376890	Creature Lie Down Sad_03	C:\Users\seth\Desktop\Wwise\FeedMe\.cache\Windows\SFX\Creature Lie Down Sad_03_E843A46D.wem		\Actor-Mixer Hierarchy\Default Work Unit\Creature_Lie_Down_Sad\Creature Lie Down Sad_03		7517
	423207173	Creature Dance_02	C:\Users\seth\Desktop\Wwise\FeedMe\.cache\Windows\SFX\Creature Dance_02_E843A46D.wem		\Actor-Mixer Hierarchy\Default Work Unit\Creature_Dance\Creature Dance_02		21192
	423358498	Creature Eating_01	C:\Users\seth\Desktop\Wwise\FeedMe\.cache\Windows\SFX\Creature Eating_01_E843A46D.wem		\Actor-Mixer Hierarchy\Default Work Unit\Creature_Eating\Creature Eating_01		21167
	435746695	Creature Eating_04	C:\Users\seth\Desktop\Wwise\FeedMe\.cache\Windows\SFX\Creature Eating_04_E843A46D.wem		\Actor-Mixer Hierarchy\Default Work Unit\Creature_Eating\Creature Eating_04		21405
	438367107	Creature Laugh_01	C:\Users\seth\Desktop\Wwise\FeedMe\.cache\Windows\SFX\Creature Laugh_01_E843A46D.wem		\Actor-Mixer Hierarchy\Default Work Unit\Creature_Laugh\Creature Laugh_01		18375
	511541830	Creature Walk_06	C:\Users\seth\Desktop\Wwise\FeedMe\.cache\Windows\SFX\Creature Walk_06_E843A46D.wem		\Actor-Mixer Hierarchy\Default Work Unit\Creature_Walk\Creature Walk_06		8199
	541627072	Creature Roll_01	C:\Users\seth\Desktop\Wwise\FeedMe\.cache\Windows\SFX\Creature Roll_01_E843A46D.wem		\Actor-Mixer Hierarchy\Default Work Unit\Creature_Roll\Creature Roll_01		7278
	557205390	Creature_Press_Button_01	C:\Users\seth\Desktop\Wwise\FeedMe\.cache\Windows\SFX\Button_01_E843A46D.wem		\Actor-Mixer Hierarchy\Default Work Unit\Creature_Press_Button_01		3304
	561083122	Creature Idle_02	C:\Users\seth\Desktop\Wwise\FeedMe\.cache\Windows\SFX\Creature Idle_02_E843A46D.wem		\Actor-Mixer Hierarchy\Default Work Unit\Creature_Idle\Creature Idle_02		15276
	664207618	Creature Lie Down_02	C:\Users\seth\Desktop\Wwise\FeedMe\.cache\Windows\SFX\Creature Lie Down_02_E843A46D.wem		\Actor-Mixer Hierarchy\Default Work Unit\Creature_Lie_Down\Creature Lie Down_02		5693
	668553253	Creature_Drop_01	C:\Users\seth\Desktop\Wwise\FeedMe\.cache\Windows\SFX\Drop Creature_02_E843A46D.wem		\Actor-Mixer Hierarchy\Default Work Unit\Creature_Drop_01		9132
	679208192	Creature Sad Idle_03	C:\Users\seth\Desktop\Wwise\FeedMe\.cache\Windows\SFX\Creature Sad Idle_03_E843A46D.wem		\Actor-Mixer Hierarchy\Default Work Unit\Creature_Sad_Idle\Creature Sad Idle_03		21004
	689258744	Creature Eating_03	C:\Users\seth\Desktop\Wwise\FeedMe\.cache\Windows\SFX\Creature Eating_03_E843A46D.wem		\Actor-Mixer Hierarchy\Default Work Unit\Creature_Eating\Creature Eating_03		29321
	712544275	Creature Dance_04	C:\Users\seth\Desktop\Wwise\FeedMe\.cache\Windows\SFX\Creature Dance_04_E843A46D.wem		\Actor-Mixer Hierarchy\Default Work Unit\Creature_Dance\Creature Dance_04		20595
	738758313	Creature Sad Idle_01	C:\Users\seth\Desktop\Wwise\FeedMe\.cache\Windows\SFX\Creature Sad Idle_01_E843A46D.wem		\Actor-Mixer Hierarchy\Default Work Unit\Creature_Sad_Idle\Creature Sad Idle_01		17025
	745213419	Creature Idle_06	C:\Users\seth\Desktop\Wwise\FeedMe\.cache\Windows\SFX\Creature Idle_06_E843A46D.wem		\Actor-Mixer Hierarchy\Default Work Unit\Creature_Idle\Creature Idle_06		15242
	782771767	Creature Pain_01	C:\Users\seth\Desktop\Wwise\FeedMe\.cache\Windows\SFX\Creature Pain_01_E843A46D.wem		\Actor-Mixer Hierarchy\Default Work Unit\Creature_Pain\Creature Pain_01		6577
	785585218	Creature Sad Idle_04	C:\Users\seth\Desktop\Wwise\FeedMe\.cache\Windows\SFX\Creature Sad Idle_04_E843A46D.wem		\Actor-Mixer Hierarchy\Default Work Unit\Creature_Sad_Idle\Creature Sad Idle_04		16243
	797224208	Creature Sad Idle_05	C:\Users\seth\Desktop\Wwise\FeedMe\.cache\Windows\SFX\Creature Sad Idle_05_E843A46D.wem		\Actor-Mixer Hierarchy\Default Work Unit\Creature_Sad_Idle\Creature Sad Idle_05		12161
	819188911	Creature_Dump_01	C:\Users\seth\Desktop\Wwise\FeedMe\.cache\Windows\SFX\Drop Creature_01_E843A46D.wem		\Actor-Mixer Hierarchy\Default Work Unit\Creature_Dump_01		10773
	850484046	Creature Lie Down Sad_02	C:\Users\seth\Desktop\Wwise\FeedMe\.cache\Windows\SFX\Creature Lie Down Sad_02_E843A46D.wem		\Actor-Mixer Hierarchy\Default Work Unit\Creature_Lie_Down_Sad\Creature Lie Down Sad_02		8457
	857629029	Creature Laugh_02	C:\Users\seth\Desktop\Wwise\FeedMe\.cache\Windows\SFX\Creature Laugh_02_E843A46D.wem		\Actor-Mixer Hierarchy\Default Work Unit\Creature_Laugh\Creature Laugh_02		19776
	892351038	Creature Dance_03	C:\Users\seth\Desktop\Wwise\FeedMe\.cache\Windows\SFX\Creature Dance_03_E843A46D.wem		\Actor-Mixer Hierarchy\Default Work Unit\Creature_Dance\Creature Dance_03		20288
	951119345	Creature Walk_07	C:\Users\seth\Desktop\Wwise\FeedMe\.cache\Windows\SFX\Creature Walk_07_E843A46D.wem		\Actor-Mixer Hierarchy\Default Work Unit\Creature_Walk\Creature Walk_07		7849
	980601541	Creature Idle_08	C:\Users\seth\Desktop\Wwise\FeedMe\.cache\Windows\SFX\Creature Idle_08_E843A46D.wem		\Actor-Mixer Hierarchy\Default Work Unit\Creature_Idle\Creature Idle_08		6498
	986634846	Creature Sad Idle_06	C:\Users\seth\Desktop\Wwise\FeedMe\.cache\Windows\SFX\Creature Sad Idle_06_E843A46D.wem		\Actor-Mixer Hierarchy\Default Work Unit\Creature_Sad_Idle\Creature Sad Idle_06		17270
	992073196	Electric Shock_02	C:\Users\seth\Desktop\Wwise\FeedMe\.cache\Windows\SFX\Electric Shock_02_E843A46D.wem		\Actor-Mixer Hierarchy\Default Work Unit\Electric Shock_02		9627
	994950409	Creature Roll_03	C:\Users\seth\Desktop\Wwise\FeedMe\.cache\Windows\SFX\Creature Roll_03_E843A46D.wem		\Actor-Mixer Hierarchy\Default Work Unit\Creature_Roll\Creature Roll_03		6899
	1001484556	Creature Idle_05	C:\Users\seth\Desktop\Wwise\FeedMe\.cache\Windows\SFX\Creature Idle_05_E843A46D.wem		\Actor-Mixer Hierarchy\Default Work Unit\Creature_Idle\Creature Idle_05		6999
	1005599821	Red Light_01	C:\Users\seth\Desktop\Wwise\FeedMe\.cache\Windows\SFX\Red Light_01_E843A46D.wem		\Actor-Mixer Hierarchy\Default Work Unit\Red Light_01		11858
	1029455161	Creature Walk_05	C:\Users\seth\Desktop\Wwise\FeedMe\.cache\Windows\SFX\Creature Walk_05_E843A46D.wem		\Actor-Mixer Hierarchy\Default Work Unit\Creature_Walk\Creature Walk_05		8297

Streamed Audio	ID	Name	Audio source file	Generated audio file	Wwise Object Path	Notes
	561095377	Feed_Me_Music_01	C:\Users\seth\Desktop\Wwise\FeedMe\.cache\Windows\SFX\Feed_Me_Music_01_22F55014.wem	561095377.wem	\Interactive Music Hierarchy\Default Work Unit\MUSIC\Feed_Me_Music_01\Feed_Me_Music_01	

