﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;

public class CircularBuffer<T> {
	private List<T> elements;
	private int curr = 0;
	private int length = 0;

	public CircularBuffer(int size) {
		elements = new List<T>();
		for(int i = 0; i < size; ++i) {
			elements.Add(default(T));
		}
		length = size;
	}

	public T Put(T item) {
		curr = (curr + 1) % length;
		elements[curr] =  item;
		return item;
	}

	public T Replace(T item, int offset = 0) {
		offset = curr - offset;
		if(offset < 0) {
			offset = length + offset;
		}
		elements[offset] = item;
		return item;
	}

	public T Get(int offset = 0) {
		offset = curr - offset;
		if(offset < 0) {
			offset = length + offset;
		}
		return elements[offset];
	}

	public T GetNext() {
		T next = Get();
		curr = (curr + 1) % length;
		return next;
	}

	override public string ToString() {
		string ax = "";
		foreach (T item in elements) {
			ax += (item.ToString() + "; ");
		}
		return curr.ToString() + ":" + ax;
	}

	public string Dump() {
		string ax = "";
		for (int i = 0; i < length; ++i) {
			ax = Get(i) + " " + ax;
		}
		return ax;
	}
}
